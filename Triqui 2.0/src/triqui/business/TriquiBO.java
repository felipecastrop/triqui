package triqui.business;

import java.util.ArrayList;

import triqui.data.TriquiDAO;
import triqui.model.Casilla;
import triqui.model.Ficha;
import triqui.model.Juego;
import triqui.model.Jugada;
import triqui.model.Jugador;
import triqui.model.Tablero;

public class TriquiBO {

//    private final Juego game = new Juego();
//    private final Tablero tablero = new Tablero();
    public Ficha buscarFicha(Juego game, String txtFicha) { // Metodo para buscar una ficha en el ArrayList de fichas
        Ficha ficha = null;
        if (txtFicha.length() > 4) {
            ficha = null;
            return ficha;
        } else {
            for (int pos = 0; pos < game.getFichas().size(); pos++) { // Ciclo para recorrer el ArrayList
                if (game.getFichas().get(pos).getForma() == txtFicha.charAt(0)) { // Compara la forma con el primer caracter del String txtficha
                    if (game.getFichas().get(pos).getTamaño() == txtFicha.charAt(1)) { // Compara el tama�o con el segundocaracter del String txtficha
                        if (game.getFichas().get(pos).getColor() == txtFicha.charAt(2)) { // Compara el color con el tercer caracter del String txtficha
                            if (game.getFichas().get(pos).getPunto() == txtFicha.charAt(3)) { // Compara punto con el cuarto caracter del String txtficha
                                ficha = game.getFichas().get(pos);
                                game.getFichas().remove(pos);
                            }
                        }
                    }
                }
            }
        }
        return ficha; // Retorna la ficha
    }

    public Casilla buscarCasilla(Tablero tablero, int pos) { // Metodo para buscar una casilla en la matriz casillas
        Casilla casilla = null;
        for (int fil = 0; fil < 5; fil++) {
            for (int col = 0; col < 5; col++) {
                if (tablero.getCasillas()[fil][col].getPosicion() == pos) {
                    casilla = tablero.getCasillas()[fil][col];
                }
            }
        }
        return casilla;
    }

    public boolean verificarTriqui(Tablero tablero) {

        boolean triqui = false;
        int contTH = 0;
        int contFH = 0;
        int contCH = 0;
        int contPH = 0; // Contadores para triqui horizontal
        int contTV = 0;
        int contFV = 0;
        int contCV = 0;
        int contPV = 0; // Contadores para triqui vertical
        int contTDP = 0;
        int contFDP = 0;
        int contCDP = 0;
        int contPDP = 0; // Contadores para triqui diagonal principal
        int contTDS = 0;
        int contFDS = 0;
        int contCDS = 0;
        int contPDS = 0; // Contadores para triqui diagonal secundaria

        // Busca si hay triqui horizontal
        for (int fil = 0; fil < tablero.getCasillas().length; fil++) {
            for (int col = 1; col < tablero.getCasillas().length; col++) {
                if (tablero.getCasillas()[fil][0].getFicha() != null) {
                    if (tablero.getCasillas()[fil][col].getFicha() != null) {
                        if (tablero.getCasillas()[fil][0].getFicha().getForma() == tablero.getCasillas()[fil][col]
                                .getFicha().getForma()) {
                            contFH++;
                        }
                        if (tablero.getCasillas()[fil][0].getFicha().getTamaño() == tablero.getCasillas()[fil][col]
                                .getFicha().getTamaño()) {
                            contTH++;
                        }
                        if (tablero.getCasillas()[fil][0].getFicha().getColor() == tablero.getCasillas()[fil][col]
                                .getFicha().getColor()) {
                            contCH++;
                        }
                        if (tablero.getCasillas()[fil][0].getFicha().getPunto() == tablero.getCasillas()[fil][col]
                                .getFicha().getPunto()) {
                            contPH++;
                        }
                    }
                }
            }
            if (contTH == 4 || contFH == 4 || contCH == 4 || contPH == 4) { // Verifica si hay triqui horizontal
                triqui = true;
            }
        }
        contTH = 0;
        contFH = 0;
        contCH = 0;
        contPH = 0;

        // Busca si hay triqui vertical
        for (int col = 0; col < tablero.getCasillas().length; col++) {
            for (int fil = 1; fil < tablero.getCasillas().length; fil++) {
                if (tablero.getCasillas()[0][col].getFicha() != null) {
                    if (tablero.getCasillas()[fil][col].getFicha() != null) {
                        if (tablero.getCasillas()[0][col].getFicha().getForma() == tablero.getCasillas()[fil][col]
                                .getFicha().getForma()) {
                            contFV++;
                        }
                        if (tablero.getCasillas()[0][col].getFicha().getTamaño() == tablero.getCasillas()[fil][col]
                                .getFicha().getTamaño()) {
                            contTV++;
                        }
                        if (tablero.getCasillas()[0][col].getFicha().getColor() == tablero.getCasillas()[fil][col]
                                .getFicha().getColor()) {
                            contCV++;
                        }
                        if (tablero.getCasillas()[0][col].getFicha().getPunto() == tablero.getCasillas()[fil][col]
                                .getFicha().getPunto()) {
                            contPV++;
                        }
                    }
                }
            }
            if (contTV == 4 || contFV == 4 || contCV == 4 || contPV == 4) { // Verifica si hay triqui vertical
                triqui = true;
            }
        }
        contTV = 0;
        contFV = 0;
        contCV = 0;
        contPV = 0;

        // Busca si hay triqui en la diagonal principal
        for (int pos = 1; pos < tablero.getCasillas().length; pos++) {
            if (tablero.getCasillas()[0][0].getFicha() != null) {
                if (tablero.getCasillas()[pos][pos].getFicha() != null) {
                    if (tablero.getCasillas()[0][0].getFicha().getForma() == tablero.getCasillas()[pos][pos].getFicha()
                            .getForma()) {
                        contFDP++;
                    }
                    if (tablero.getCasillas()[0][0].getFicha().getTamaño() == tablero.getCasillas()[pos][pos].getFicha()
                            .getTamaño()) {
                        contTDP++;
                    }
                    if (tablero.getCasillas()[0][0].getFicha().getColor() == tablero.getCasillas()[pos][pos].getFicha()
                            .getColor()) {
                        contCDP++;
                    }
                    if (tablero.getCasillas()[0][0].getFicha().getPunto() == tablero.getCasillas()[pos][pos].getFicha()
                            .getPunto()) {
                        contPDP++;
                    }
                }
            }
        }
        if (contTDP == 4 || contFDP == 4 || contCDP == 4 || contPDP == 4) { // Verifica si hay triqui en la diagonal principal
            triqui = true;
            return triqui;
        }
        contTDP = 0;
        contFDP = 0;
        contCDP = 0;
        contPDP = 0;

        // Busca si hay triqui en la diagonal secundaria
        for (int fil = 1; fil < tablero.getCasillas().length; fil++) {
            for (int col = 0; col < tablero.getCasillas().length; col++) {
                if (tablero.getCasillas()[0][tablero.getCasillas().length - 1].getFicha() != null) {
                    if (fil + col == tablero.getCasillas().length - 1) {
                        if (tablero.getCasillas()[fil][col].getFicha() != null) {
                            if (tablero.getCasillas()[0][tablero.getCasillas().length - 1].getFicha()
                                    .getForma() == tablero.getCasillas()[fil][col].getFicha().getForma()) {
                                contFDS++;
                            }
                            if (tablero.getCasillas()[0][tablero.getCasillas().length - 1].getFicha()
                                    .getTamaño() == tablero.getCasillas()[fil][col].getFicha().getTamaño()) {
                                contTDS++;
                            }
                            if (tablero.getCasillas()[0][tablero.getCasillas().length - 1].getFicha()
                                    .getColor() == tablero.getCasillas()[fil][col].getFicha().getColor()) {
                                contCDS++;
                            }
                            if (tablero.getCasillas()[0][tablero.getCasillas().length - 1].getFicha()
                                    .getPunto() == tablero.getCasillas()[fil][col].getFicha().getPunto()) {
                                contPDS++;
                            }
                        }
                    }
                }
            }
            if (contTDS == 4 || contFDS == 4 || contCDS == 4 || contPDS == 4) { // Verifica si hay triqui en la diagonal secundaria
                triqui = true;
            }
        }
        contTDS = 0;
        contFDS = 0;
        contCDS = 0;
        contPDS = 0;

        return triqui;
    }

    public void agregarJuego() {
        TriquiDAO dao = new TriquiDAO();
        dao.agregarJuego();
    }

    public Juego obtenerJuegos() {

        // Crea la lista de jugadas
        Juego idJuego = new Juego();

        // Crea el objeto de acceso a datos
        TriquiDAO dao = new TriquiDAO();

        // Obtiene los juegos de la base de datos
        idJuego = dao.obtenerIDJuego();

        // Retorna la lista de juegos
        return idJuego;
    }
    

    public void agregarJugada(Juego game, Jugador jugador, Ficha ficha, Casilla casilla, int validez) {
        TriquiDAO dao = new TriquiDAO();
        dao.agregarJugada(game, jugador, ficha, casilla, validez);
    }

    public ArrayList<Jugada> obtenerJugadas() {

        // Crea la lista de jugadas
        ArrayList<Jugada> jugadas = new ArrayList<Jugada>();

        // Crea el objeto de acceso a datos
        TriquiDAO dao = new TriquiDAO();

        // Obtiene las peliculas de la base de datos
        jugadas = dao.obtenerJugadas();

        // Retorna la lista de jugadas
        return jugadas;
    }

    public void agregarJugador(Juego game, Jugador jugador) {
        TriquiDAO dao = new TriquiDAO();
        dao.agregarJugador(game, jugador);
    }

    public ArrayList<Jugador> obtenerJugadores() {

        // Crea la lista de jugadores
        ArrayList<Jugador> jugadores = new ArrayList<Jugador>();

        // Crea el objeto de acceso a datos
        TriquiDAO dao = new TriquiDAO();

        // Obtiene las peliculas de la base de datos
        jugadores = dao.obtenerJugadores();

        // Retorna la lista de jugadores
        return jugadores;
    }

}
