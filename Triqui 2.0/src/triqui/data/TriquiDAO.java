package triqui.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import triqui.model.Ficha;
import triqui.model.Jugada;
import triqui.model.Casilla;
import triqui.model.Juego;
import triqui.model.Jugador;

public class TriquiDAO {

    public void agregarJuego() {
        String sql = "INSERT INTO Juego(fechaHora) VALUES(?)";
        // Se obtiene la fecha y hora
        Date date = new Date();
        Timestamp fechaHora = new Timestamp(date.getTime());
        try {
            // Obtiene la conexion a la base de datos
            Connection conexion = Conexion.obtenerConexion();

            // Crea la sentencia
            PreparedStatement statement = conexion.prepareStatement(sql);

            // Agrega los par�metros de la sentencia
            statement.setString(1, fechaHora.toLocaleString()); // La fecha y hora se coloca en formato con zona horaria y a String

            // Ejecuta la sentencia
            statement.executeUpdate();

        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
    }

    public Juego obtenerIDJuego() {

        // Construye la sentencia SQL
        String sql = "SELECT max(id) as id FROM Juego";

        // Crea la lista de jugadores
        Juego juego = new Juego();

        try {
            // Obtiene la conexi�n a la base de datos
            Connection conexion = Conexion.obtenerConexion();

            // Crea la sentencia usando la coneion
            Statement sentencia = conexion.createStatement();

            // Ejecuta la sentencia SQL
            ResultSet resultado = sentencia.executeQuery(sql);

            // Recorre los registros obtenidos
            while (resultado.next()) {

                // Crea el objeto del modelo
                juego.setId(resultado.getInt("id"));
                //juego.setFechaHora(resultado.getString("fechaHora"));               
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        // Retorna la lista
        return juego;
    }

    public void agregarJugada(Juego game, Jugador jugador, Ficha ficha, Casilla casilla, int validez) {

        // Construye la cadena SQL de inserci�n
        String sql = "INSERT INTO Jugada(juego,jugador,ficha,casilla,fechaHora,validez) VALUES(?,?,?,?,?,?)";
        // Se obtiene la fecha y hora
        Date date = new Date();
        Timestamp fechaHora = new Timestamp(date.getTime());
        try {
            // Obtiene la conexion a la base de datos
            Connection conexion = Conexion.obtenerConexion();

            // Crea la sentencia
            PreparedStatement statement = conexion.prepareStatement(sql);

            // Agrega los par�metros de la sentencia
            statement.setInt(1, game.getId());
            statement.setInt(2, jugador.getId()); // Se envia el id del Jugador
            statement.setString(3, ficha.getForma() + "" + ficha.getTamaño() + "" + ficha.getColor() + "" + ficha.getPunto()); // Se envia la ficha en formato String
            statement.setInt(4, casilla.getPosicion()); // Se envia la posicion de la casilla
            statement.setString(5, fechaHora.toLocaleString()); // La fecha y hora se coloca en formato con zona horaria y a String
            statement.setInt(6, validez);

            // Ejecuta la sentencia
            statement.executeUpdate();

        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
    }

    public ArrayList<Jugada> obtenerJugadas(Juego game) {

        // Construye la sentencia SQL 
        String sql = "SELECT id,jugador,ficha,casilla,fechaHora,validez FROM Jugada where juego = ?";
        // Crea la lista de jugadores 
        ArrayList<Jugada> jugadas = new ArrayList<Jugada>();

        try {
            // Obtiene la conexi�n a la base de datos 
            Connection conexion = Conexion.obtenerConexion();
            PreparedStatement statement = conexion.prepareStatement(sql);

            statement.setInt(1 , game.getId());
            ResultSet resultado = statement.executeQuery();

            // Recorre los registros obtenidos 
            while (resultado.next()) {
                // Crea el objeto del modelo 
                Jugada jugada = new Jugada();
                jugada.setId(resultado.getInt("id"));
                jugada.setJugador(resultado.getInt("jugador"));
                jugada.setFicha(resultado.getString("ficha"));
                jugada.setCasilla(resultado.getInt("casilla"));
                jugada.setFechaHora(resultado.getString("fechaHora"));
                jugada.setValidez(resultado.getInt("validez"));

                // Agrega el objeto a la lista 
                jugadas.add(jugada);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        // Retorna la lista 
        return jugadas;
    }

    public void agregarJugador(Juego game, Jugador jugador) {

        // Construye la cadena SQL de inserci�n
        String sql = "INSERT INTO Jugador(juego, nombre) VALUES(?,?)";

        try {
            // Obtiene la conexion a la base de datos
            Connection conexion = Conexion.obtenerConexion();

            // Crea la sentencia
            PreparedStatement statement = conexion.prepareStatement(sql);

            // Agrega los par�metros de la sentencia
            statement.setInt(1, game.getId());
            statement.setString(2, jugador.getNombre());

            // Ejecuta la sentencia
            statement.executeUpdate();

            ResultSet rs = statement.getGeneratedKeys();

            int id = 0;
            if (rs.next()) {
                id = rs.getInt(1);
                jugador.setId(id);
            }

        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
    }

    public ArrayList<Jugador> obtenerJugadores() {

        // Construye la sentencia SQL
        String sql = "SELECT id, nombre FROM Jugador";

        // Crea la lista de jugadores
        ArrayList<Jugador> jugadores = new ArrayList<Jugador>();

        try {
            // Obtiene la conexi�n a la base de datos
            Connection conexion = Conexion.obtenerConexion();

            // Crea la sentencia usando la coneion
            Statement sentencia = conexion.createStatement();

            // Ejecuta la sentencia SQL
            ResultSet resultado = sentencia.executeQuery(sql);

            // Recorre los registros obtenidos
            while (resultado.next()) {

                // Crea el objeto del modelo
                Jugador jugador = new Jugador();
                jugador.setId(resultado.getInt("id"));
                jugador.setNombre(resultado.getString("nombre"));

                // Agrega el objeto a la lista
                jugadores.add(jugador);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        // Retorna la lista
        return jugadores;
    }
}
