package triqui.model;

public class Ficha {

    private char forma;
    private char tamaño;
    private char color;
    private char punto;

    public Ficha(char f, char t, char c, char p) {
        this.forma = f;
        this.tamaño = t;
        this.color = c;
        this.punto = p;
    }

    public char getForma() {
        return forma;
    }

    public void setForma(char forma) {
        this.forma = forma;
    }

    public char getTamaño() {
        return tamaño;
    }

    public void setTamaño(char tamaño) {
        this.tamaño = tamaño;
    }

    public char getColor() {
        return color;
    }

    public void setColor(char color) {
        this.color = color;
    }

    public char getPunto() {
        return punto;
    }

    public void setPunto(char punto) {
        this.punto = punto;
    }

    @Override
    public String toString() {
        return forma + "" + tamaño + "" + color + "" + punto;
    }

}
