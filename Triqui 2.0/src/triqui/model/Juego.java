package triqui.model;

import java.util.ArrayList;

public class Juego {

    private int id;
    private String fechaHora;
    private Tablero tablero;
    private ArrayList<Ficha> fichas;

    private Jugador jugador1;
    private Jugador jugador2;
    private int puntuacion = 0;

    public Juego() {
        tablero = new Tablero();
        fichas = new ArrayList<Ficha>();
        jugador1 = new Jugador();
        jugador2 = new Jugador();
        // Se crean las 36 fichas del juego
        this.fichas.add(new Ficha('C', 'G', 'R', 'C'));
        this.fichas.add(new Ficha('C', 'G', 'R', 'S'));
        this.fichas.add(new Ficha('C', 'G', 'V', 'C'));
        this.fichas.add(new Ficha('C', 'G', 'V', 'S'));
        this.fichas.add(new Ficha('C', 'G', 'N', 'C'));
        this.fichas.add(new Ficha('C', 'G', 'N', 'S'));
        this.fichas.add(new Ficha('C', 'P', 'R', 'C'));
        this.fichas.add(new Ficha('C', 'P', 'R', 'S'));
        this.fichas.add(new Ficha('C', 'P', 'V', 'C'));
        this.fichas.add(new Ficha('C', 'P', 'V', 'S'));
        this.fichas.add(new Ficha('C', 'P', 'N', 'C'));
        this.fichas.add(new Ficha('C', 'P', 'N', 'S'));
        this.fichas.add(new Ficha('S', 'G', 'R', 'C'));
        this.fichas.add(new Ficha('S', 'G', 'R', 'S'));
        this.fichas.add(new Ficha('S', 'G', 'V', 'C'));
        this.fichas.add(new Ficha('S', 'G', 'V', 'S'));
        this.fichas.add(new Ficha('S', 'G', 'N', 'C'));
        this.fichas.add(new Ficha('S', 'G', 'N', 'S'));
        this.fichas.add(new Ficha('S', 'P', 'R', 'C'));
        this.fichas.add(new Ficha('S', 'P', 'R', 'S'));
        this.fichas.add(new Ficha('S', 'P', 'V', 'C'));
        this.fichas.add(new Ficha('S', 'P', 'V', 'S'));
        this.fichas.add(new Ficha('S', 'P', 'N', 'C'));
        this.fichas.add(new Ficha('S', 'P', 'N', 'S'));
        this.fichas.add(new Ficha('T', 'G', 'R', 'C'));
        this.fichas.add(new Ficha('T', 'G', 'R', 'S'));
        this.fichas.add(new Ficha('T', 'G', 'V', 'C'));
        this.fichas.add(new Ficha('T', 'G', 'V', 'S'));
        this.fichas.add(new Ficha('T', 'G', 'N', 'C'));
        this.fichas.add(new Ficha('T', 'G', 'N', 'S'));
        this.fichas.add(new Ficha('T', 'P', 'R', 'C'));
        this.fichas.add(new Ficha('T', 'P', 'R', 'S'));
        this.fichas.add(new Ficha('T', 'P', 'V', 'C'));
        this.fichas.add(new Ficha('T', 'P', 'V', 'S'));
        this.fichas.add(new Ficha('T', 'P', 'N', 'C'));
        this.fichas.add(new Ficha('T', 'P', 'N', 'S'));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(String fechaHora) {
        this.fechaHora = fechaHora;
    }

    public Tablero getTablero() {
        return tablero;
    }

    public void setTablero(Tablero tablero) {
        this.tablero = tablero;
    }

    public int getPuntuacion() {
        return puntuacion;
    }

    public void setPuntuacion(int puntuacion) {
        this.puntuacion = puntuacion;
    }

    public ArrayList<Ficha> getFichas() {
        return fichas;
    }

    public void setFichas(Ficha ficha) {
        this.fichas.add(ficha);
    }

    public Jugador getJugador1() {
        return jugador1;
    }

    public void setJugador1(Jugador jugador1) {
        this.jugador1 = jugador1;
    }

    public Jugador getJugador2() {
        return jugador2;
    }

    public void setJugador2(Jugador jugador2) {
        this.jugador2 = jugador2;
    }

}
