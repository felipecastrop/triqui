package triqui.model;

public class Jugada {

    private int id;
    private int jugador;
    private String ficha;
    private int casilla;
    private String fechaHora;
    private int validez;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getJugador() {
        return jugador;
    }

    public void setJugador(int jugador) {
        this.jugador = jugador;
    }

    public String getFicha() {
        return ficha;
    }

    public void setFicha(String ficha) {
        this.ficha = ficha;
    }

    public int getCasilla() {
        return casilla;
    }

    public void setCasilla(int casilla) {
        this.casilla = casilla;
    }

    public String getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(String fechaHora) {
        this.fechaHora = fechaHora;
    }

    public int getValidez() {
        return validez;
    }

    public void setValidez(int validez) {
        this.validez = validez;
    }

    @Override
    public String toString() {
        return this.id + " | " + this.jugador + " | " + this.ficha + " | " + this.casilla + " | " + this.fechaHora+ " | " + this.validez;
    }
}
