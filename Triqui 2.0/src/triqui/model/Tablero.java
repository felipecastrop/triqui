package triqui.model;

public class Tablero {

    private Casilla[][] casillas;

    public Tablero() {
        casillas = new Casilla[5][5];
        int posicion = 1;
        for (int fila = 0; fila < 5; fila++) {
            for (int col = 0; col < 5; col++) {
                this.casillas[fila][col] = new Casilla(posicion);
                posicion++;
            }
        }
    }

    public Casilla[][] getCasillas() {
        return casillas;
    }

    public void setCasillas(Casilla[][] casillas) {
        this.casillas = casillas;
    }

}
