package triqui.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion {
	
	private static String url = "jdbc:sqlite:BDTriqui.db";
	  
	public static Connection obtenerConexion() {
		// SQLite connection string
		Connection conexion = null;
		try {
			// Ejecuta la conexion a la base de datos
			conexion = DriverManager.getConnection(url);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		// Retorna la conexi�n a la base de datos
		return conexion;
	}
}
