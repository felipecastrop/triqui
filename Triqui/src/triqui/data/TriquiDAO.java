package triqui.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import triqui.model.Ficha;
import triqui.model.Juego;
import triqui.model.Casilla;
import triqui.model.Jugador;

public class TriquiDAO {

	public void agregarJugada(Jugador jugador, Ficha ficha, Casilla casilla, int validez) {

		// Construye la cadena SQL de inserci�n
		String sql = "INSERT INTO Jugada(jugador,ficha,casilla,fechaHora,validez) VALUES(?,?,?,?,?)";
		// Se obtiene la fecha y hora
		Date date = new Date();
		Timestamp fechaHora = new Timestamp(date.getTime());
		try {
			// Obtiene la conexion a la base de datos
			Connection conexion = Conexion.obtenerConexion();

			// Crea la sentencia
			PreparedStatement statement = conexion.prepareStatement(sql);

			// Agrega los par�metros de la sentencia
			statement.setInt(1, jugador.getId()); // Se envia el id del Jugador
			statement.setString(2,ficha.getForma() + "" + ficha.getTama�o() + "" + ficha.getColor() + "" + ficha.getPunto()); // Se envia la ficha en formato String
			statement.setInt(3, casilla.getPosicion()); // Se envia la posicion de la casilla
			statement.setString(4, fechaHora.toLocaleString()); // La fecha y hora se coloca en formato con zona horaria y a String
			statement.setInt(5, validez);

			// Ejecuta la sentencia
			statement.executeUpdate();

		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
	}

	/*
	 * public ArrayList<Juego> obtenerJugadas() {
	 * 
	 * // Construye la sentencia SQL String sql =
	 * "SELECT id, jugador,ficha,casilla,fechaHora FROM Jugadas"; // // Crea la
	 * lista de jugadores ArrayList<Juego> jugadas = new ArrayList<Juego>();
	 * 
	 * try { // Obtiene la conexi�n a la base de datos Connection conexion =
	 * Conexion.obtenerConexion();
	 * 
	 * // Crea la sentencia usando la coneion Statement sentencia =
	 * conexion.createStatement();
	 * 
	 * // Ejecuta la sentencia SQL ResultSet resultado =
	 * sentencia.executeQuery(sql);
	 * 
	 * // Recorre los registros obtenidos while (resultado.next()) {
	 * 
	 * // Crea el objeto del modelo Juego jugada = new Juego();
	 * jugada.(resultado.getString("ficha");
	 * jugada.setNombre(resultado.getString("nombre"));
	 * 
	 * // Agrega el objeto a la lista jugadas.add(jugada); } } catch (SQLException
	 * e) { System.out.println(e.getMessage()); }
	 * 
	 * // Retorna la lista return jugadas; }
	 */

	public void agregarJugador(Jugador jugador) {

		// Construye la cadena SQL de inserci�n
		String sql = "INSERT INTO Jugador(nombre) VALUES(?)";

		try {
			// Obtiene la conexion a la base de datos
			Connection conexion = Conexion.obtenerConexion();

			// Crea la sentencia
			PreparedStatement statement = conexion.prepareStatement(sql);

			// Agrega los par�metros de la sentencia
			statement.setString(1, jugador.getNombre());

			// Ejecuta la sentencia
			statement.executeUpdate();

		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
	}

	public ArrayList<Jugador> obtenerJugadores() {

		// Construye la sentencia SQL
		String sql = "SELECT id, nombre FROM Jugador";

		// Crea la lista de jugadores
		ArrayList<Jugador> jugadores = new ArrayList<Jugador>();

		try {
			// Obtiene la conexi�n a la base de datos
			Connection conexion = Conexion.obtenerConexion();

			// Crea la sentencia usando la coneion
			Statement sentencia = conexion.createStatement();

			// Ejecuta la sentencia SQL
			ResultSet resultado = sentencia.executeQuery(sql);

			// Recorre los registros obtenidos
			while (resultado.next()) {

				// Crea el objeto del modelo
				Jugador jugador = new Jugador();
				jugador.setId(resultado.getInt("id"));
				jugador.setNombre(resultado.getString("nombre"));

				// Agrega el objeto a la lista
				jugadores.add(jugador);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

		// Retorna la lista
		return jugadores;
	}
}
