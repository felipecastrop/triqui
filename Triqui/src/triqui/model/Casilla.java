package triqui.model;

public class Casilla {
	private int posicion;
	private Ficha ficha;

	public Casilla(int posicion) {
		this.posicion = posicion;
	}

	public Ficha getFicha() {
		return ficha;
	}

	public void setFicha(Ficha ficha) {
		this.ficha = ficha;
	}

	public int getPosicion() {
		return posicion;
	}

}
