package triqui.model;

public class Ficha {
	private char forma;
	private char tama�o;
	private char color;
	private char punto;

	public Ficha(char f, char t, char c, char p) {
		this.forma = f;
		this.tama�o = t;
		this.color = c;
		this.punto = p;
	}

	public char getForma() {
		return forma;
	}

	public void setForma(char forma) {
		this.forma = forma;
	}

	public char getTama�o() {
		return tama�o;
	}

	public void setTama�o(char tama�o) {
		this.tama�o = tama�o;
	}

	public char getColor() {
		return color;
	}

	public void setColor(char color) {
		this.color = color;
	}

	public char getPunto() {
		return punto;
	}

	public void setPunto(char punto) {
		this.punto = punto;
	}

	@Override
	public String toString() {
		return forma + "" + tama�o + "" + color + "" + punto;
	}

}
