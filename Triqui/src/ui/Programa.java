package ui;

import java.util.ArrayList;
import java.util.Scanner;

import triqui.business.TriquiBO;
import triqui.model.Ficha;
import triqui.model.Juego;
import triqui.model.Jugador;
import triqui.model.Tablero;

public class Programa {
	private static Scanner s;

	public static void main(String[] args) {

		s = new Scanner(System.in);
		Juego game = new Juego();
		Jugador player1 = new Jugador();
		Jugador player2 = new Jugador();
		TriquiBO bo = new TriquiBO();
		int opcion = 0;
		int opc = 0;
		int posicion = 0;

		do {
			System.out.println("JUEGO TRIQUI");
			System.out.println("Digite 1 para nuevo juego" + "\n" + "Digite 2 para salir");
			opcion = s.nextInt();

			if (opcion == 1) {
				game = new Juego();
				Tablero tablero = game.getTablero();
				boolean triqui = false;
				boolean rendirse = false;
				boolean empate = false;
				int jugadas = 0;

				System.out.print("Ingrese el nombre del primer jugador: ");
				player1.setNombre(s.next());
				game.setJugador1(player1);
				bo.agregarJugador(player1);

				System.out.print("Ingrese el nombre del segundo jugador: ");
				player2.setNombre(s.next());
				game.setJugador1(player2);
				bo.agregarJugador(player2);

				ArrayList<Jugador> jugadores = new ArrayList<Jugador>();
				jugadores = bo.obtenerJugadores();
				
				// Relaciona los jugadores con la Base de Datos y se asigna el ID
				for (Jugador jugador: jugadores) {
					System.out.println(jugador);
					if(jugador.getId() % 2 != 0) {
						player1.setId(jugador.getId());
					}else {
						player2.setId(jugador.getId());
					}
				}
				
				do {
					int turno = 1;
					do {
						System.out.println();
						System.out.println("El turno es de " + player1.getNombre());
						System.out.println();
						imprimirTablero(tablero);

						System.out.println("Digite 1 para jugar" + "\n" + "Digite 2 para rendirse");
						opc = s.nextInt();

						if (opc != 1) {
							if (opc != 2) {
								System.out.println("****************************");
								System.out.println("La informacion es incorrecta");
								System.out.println("****************************");
							}
						}

						if (opc == 1) {

							System.out.print("La fichas disponibles son: " + game.getFichas());
							System.out.println();

							System.out.print("Digite la ficha que desea jugar: ");
							Ficha tempFicha = bo.buscarFicha(s.next());

							if (tempFicha == null) {
								System.out.println("***************************************");
								System.out.println("La ficha no existe o ya se ha utilizado");
								System.out.println("***************************************");

							} else {
								System.out.print("Digite la posici�n en la que desea jugar: ");
								posicion = s.nextInt();

								if (bo.buscarCasilla(posicion) != null) {
									if (bo.buscarCasilla(posicion).getFicha() == null) {
										bo.buscarCasilla(posicion).setFicha(tempFicha);
										bo.agregarJugada(player1, tempFicha, bo.buscarCasilla(posicion),1);
										
										jugadas = jugadas++;
										turno = 2;
									} else {
										System.out.println("***********************");
										System.out.println("La casilla esta ocupada");
										System.out.println("***********************");
										bo.agregarJugada(player1, tempFicha, bo.buscarCasilla(posicion),0);
									}
								} else {
									System.out.println("*******************");
									System.out.println("La casilla no exite");
									System.out.println("*******************");
								}
							}
						}
						if (opc == 2) {
							System.out.println("El jugador " + player1.getNombre() + " se a rendido");
							System.out.println();
							rendirse = true;
							break;
						}
						if (bo.verificarTriqui(tablero) == true) {
							triqui = true;
							System.out.println();
							System.out.println("TRIQUI!!");
							System.out.println("El jugador " + player1.getNombre() + " ha ganado");
							imprimirTablero(tablero);
							break;
						}

					} while (turno == 1);

					if (triqui == true || rendirse == true) {
						break;
					}

					do {
						System.out.println();
						System.out.println("El turno es de " + player2.getNombre());
						System.out.println();
						imprimirTablero(tablero);

						System.out.println("Digite 1 para jugar" + "\n" + "Digite 2 para rendirse");
						opc = s.nextInt();

						if (opc != 1) {
							if (opc != 2) {
								System.out.println("****************************");
								System.out.println("La informacion es incorrecta");
								System.out.println("****************************");
							}
						}

						if (opc == 1) {

							System.out.print("La fichas disponibles son: " + game.getFichas());
							System.out.println();

							System.out.print("Digite la ficha que desea jugar: ");
							Ficha tempFicha = bo.buscarFicha(s.next());

							if (tempFicha == null) {
								System.out.println("***************************************");
								System.out.println("La ficha no existe o ya se ha utilizado");
								System.out.println("***************************************");
							} else {
								System.out.print("Digite la posici�n en la que desea jugar: ");
								posicion = s.nextInt();

								if (bo.buscarCasilla(posicion) != null) {
									if (bo.buscarCasilla(posicion).getFicha() == null) {
										bo.buscarCasilla(posicion).setFicha(tempFicha);
										bo.agregarJugada(player2, tempFicha, bo.buscarCasilla(posicion),1);
										
										jugadas = jugadas++;
										turno = 1;
									} else {
										System.out.println("***********************");
										System.out.println("La casilla esta ocupada");
										System.out.println("***********************");
										bo.agregarJugada(player2, tempFicha, bo.buscarCasilla(posicion),0);
									}
								} else {
									System.out.println("*******************");
									System.out.println("La casilla no exite");
									System.out.println("*******************");
								}
							}
						}
						if (opc == 2) {
							System.out.println("El jugador " + player2.getNombre() + " se a rendido");
							System.out.println();
							rendirse = true;
							break;
						}
						if (bo.verificarTriqui(tablero) == true) {
							triqui = true;
							System.out.println();
							System.out.println("TRIQUI!!");
							System.out.println("El jugador " + player2.getNombre() + " ha ganado");
							imprimirTablero(tablero);
							break;
						}
					} while (turno == 2);

					
					if (jugadas == 25) {
						System.out.println("*************************");
						System.out.println("Todas las casillas estan llenas" + "\n" + "ES UN EMPATE!!");
						System.out.println("*************************");
						empate =true;
					}
					
				} while (triqui == false && rendirse == false && empate == false);
				
			} else if (opcion == 2) {
				System.exit(0);
				
			} else {
				System.out.println("*************************");
				System.out.println("Ingrese una opci�n valida");
				System.out.println("*************************");
			}
		} while (opcion != 1 || opcion != 2);
	}

	public static void imprimirTablero(Tablero tablero) {
		for (int n = 0; n < tablero.getCasillas().length; n++) {
			for (int m = 0; m < tablero.getCasillas().length; m++) {
				System.out.print("| ");
				// Establece si el tablero esta en uso
				if (tablero.getCasillas()[n][m].getFicha() != null) {
					// Ilustra la ficha
					System.out.print(tablero.getCasillas()[n][m].getFicha() + " ");
				} else {
					// Muestra la posicion de la ficha
					if (tablero.getCasillas()[n][m].getPosicion() <= 9) {
						System.out.print("0" + tablero.getCasillas()[n][m].getPosicion() + " ");
					} else {
						System.out.print(tablero.getCasillas()[n][m].getPosicion() + " ");
					}

				}
			}
			System.out.println("|");
			System.out.println("---------------------------");
		}
	}
}
